/**
 * check_iniparser.c - tests iniparser library functionality
 *
 * Copyright 2018 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */

#include "iniparser.h"
#include <stdlib.h>
#include <check.h>

START_TEST(test_iniparser) {
    // Load our SelectMonitor 
    dictionary *ecm_dict = iniparser_load("SelectMonitor.ini");
    ck_assert_ptr_nonnull(ecm_dict);

    // Look up a key for a known value
    const char * value = iniparser_getstring(ecm_dict, "7332:BatteryVoltageAddress", NULL);
    ck_assert_str_eq(value, "1335");

    // Free the dictionary
    iniparser_freedict(ecm_dict);
}
END_TEST

Suite * test_suite(void) {
    Suite *s;
    TCase *tc_core;

    s = suite_create("iniparser");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_iniparser);
    suite_add_tcase(s, tc_core);

    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
