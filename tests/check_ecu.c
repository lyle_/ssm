/**
 * check_ssm.c - tests SSM interface functionality
 *
 * Copyright by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
#define _GNU_SOURCE // for asprintf
#include "ecu.h"
#include "logging.h"
#include "prefs.h"
#include <check.h>
#include <stdlib.h>

// Test string handling
START_TEST(test_strings) {
    int romId = 7549526; // hex 733256
    char *romIdHex;
    asprintf(&romIdHex, "%04X", romId);
    ck_assert_str_eq(romIdHex, "733256");

    // Parse ROM ID string
    romIdHex = parseRomIdString(romId);
    ck_assert_str_eq(romIdHex, "7332");
}
END_TEST

Suite * test_suite(void) {
    Suite *s;
    TCase *tc_strings;

    s = suite_create("ecu");

    /* String handling test case */
    tc_strings = tcase_create("Strings");

    tcase_add_test(tc_strings, test_strings);
    suite_add_tcase(s, tc_strings);

    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = test_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
