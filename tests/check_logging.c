/**
 * check_logging.c - tests logging functionality
 *
 * Copyright 2018 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
#include "logging.h"
#include <check.h>
#include <stdlib.h>


START_TEST(test_logging) {
	// We should not crash if we don't call init_logs first
	log_message("hello", DEBUG);
}
END_TEST

Suite * test_suite(void) {
	Suite *s;
	TCase *tc_core;

	s = suite_create("logging");

	/* Core test case */
	tc_core = tcase_create("Core");

	tcase_add_test(tc_core, test_logging);
	suite_add_tcase(s, tc_core);

	return s;
}

int main(void) {
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = test_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
