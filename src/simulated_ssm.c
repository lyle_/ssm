/**
 * simulated_ssm.c - A simulated version of the Subaru Select Monitor
 *  communications library useful for development without an actual ECU connection.
 *
 * Copyright 2007, 2018 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
#include "logging.h"
#include "ssm.h"
#include <stdio.h>
#include <time.h>

int ssm_open(char *device) {
    // log_message("ssm_open(): Simulating UI, no ECU connection will be established", INFO);
    return 0;
}

int ssm_reset() {
    return 0;
}

int ssm_query_ecu(int address, int *buffer, int n) {
    // Introduce a bit of delay to simulate actual ECU reads
    struct timespec s;
    s.tv_sec = 0;
    s.tv_nsec = 100000000L;
    nanosleep(&s, NULL);
    *buffer = 0;
    return 0;
}

int ssm_current(int *address, int *data) {
    return 0;
}

int ssm_close() {
    return 0;
}

int ssm_write_ecu(int address,unsigned char data) {
    return 0;
}

// Set the romid to 7332 (1993-1994 naturally aspirated)
int ssm_romid_ecu(int *romid) {
	*romid = 0x7332;
    return 0;
}
