/**
 * ecu.c - Functions to populate the data structure used to look up and display ECU
 * parameters.
 *
 * Copyright 2019 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
#define _GNU_SOURCE // for asprintf
#include "ecu.h"
#include "iniparser.h"
#include "logging.h"
#include "ssm.h"
#include <stdio.h>
#include <stdlib.h>

static int romId;
static char *romIdString;
static const _ecu *ecu;

#define MSG_BUFSIZE 250
char ecu_msg[MSG_BUFSIZE];

/*
 * Loads the provided key from the ECU address dictionary and returns an
 * initialized ecu_param.
 */
ecu_param init_param(dictionary *ecm_dict, char *paramKey, char *description) {
    // The key used to look up the ROM address in the INI file is "ROM_ID:PARAM_KEY",
    // e.g. "7332:BatteryVoltageAddress"
    char *key; // the key used to look up the ROM address in the INI file
    asprintf(&key, "%s:%s", romIdString, paramKey);

    ecu_param param = { .description = description };

    // Look up the ROM address for this ECU parameter from the INI file
    const char *address_str = iniparser_getstring(ecm_dict, key, "-1");
    param.romAddress = strtol(address_str, NULL, 16);

    return param;
}

char *parseRomIdString(int romId) {
    asprintf(&romIdString, "%04X", romId);
    // Truncate first four digits
    romIdString[4] = 0;
    return romIdString;
}

_ecu *load_ecu_settings() {
    // Load ECU-specific mappings
    int rc = ssm_romid_ecu(&romId);
    if (rc != 0) {
        snprintf(ecu_msg, MSG_BUFSIZE, "ssm_romid_ecu returned %d", rc);
        log_message(ecu_msg, ERROR);
        fprintf(stderr, "%s\n", ecu_msg);
        perror("Error Details");
        exit(1);
    }
    romIdString = parseRomIdString(romId);

    // Based on the ROM ID detected by `ssm_romid_ecu`, load a section of
    // config file and destroy the rest.
    snprintf(ecu_msg, MSG_BUFSIZE, "Running ROM ID %s (%d)", romIdString, romId);
    log_message(ecu_msg, INFO);

    // Load our SelectMonitor config file
    dictionary *ecm_dict = iniparser_load("SelectMonitor.ini");

    if (ecu) {
        ecu_free();
    }
    _ecu *ecu = malloc(sizeof *ecu);
    ecu->airFuelCorrection = init_param(ecm_dict, "AFCorrectionAddress", "Air/fuel correction");
    ecu->atmosphericPressure = init_param(ecm_dict, "AtmosphericPressureAddress", "Atmospheric pressure");
    ecu->batteryVoltage = init_param(ecm_dict, "BatteryVoltageAddress", "Battery voltage");
    ecu->boostSolenoidDutyCycle = init_param(ecm_dict, "BoostSolenoidDutyCycleAddress", "Boost solenoid duty cycle");
    ecu->coolantTemp = init_param(ecm_dict, "CoolantTempAddress", "Coolant temp");
    ecu->engineLoad = init_param(ecm_dict, "EngineLoadAddress", "Engine load");
    ecu->ignitionAdvance = init_param(ecm_dict, "IgnitionAdvanceAddress", "Ignition advance");
    ecu->injectorPulseWidth = init_param(ecm_dict, "InjectorPulseWidthAddress", "Injector pulse width");
    ecu->isuDutyValve = init_param(ecm_dict, "ISUDutyValveAddress", "ISU duty valve");
    ecu->knockCorrection = init_param(ecm_dict, "KnockCorrectionAddress", "Knock correction");
    ecu->manifoldPressure = init_param(ecm_dict, "ManifoldPressureAddress", "Manifold pressure");
    ecu->massAirFlow = init_param(ecm_dict, "AirflowSensorAddress", "Airflow sensor");
    ecu->o2Average = init_param(ecm_dict, "O2AverageAddress", "O2 average");
    ecu->o2Max = init_param(ecm_dict, "O2MinimumAddress", "O2 maximum");
    ecu->o2Min = init_param(ecm_dict, "O2MaximumAddress", "O2 minimum");
    ecu->rpm = init_param(ecm_dict, "EngineSpeedAddress", "RPM");
    ecu->throttlePosition = init_param(ecm_dict, "ThrottlePositionAddress", "Throttle position");
    ecu->vehicleSpeed = init_param(ecm_dict, "VehicleSpeedAddress", "Vehicle speed");

    iniparser_freedict(ecm_dict);

    return ecu;
}

void ecu_free(const _ecu *ecu) {
    free((_ecu *)ecu);
}
