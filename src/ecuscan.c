/**
 * ecuscan.c - View and log ECU parameters
 *
 * Copyright 2007, 2018 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
#include "curses.h"
#include "logging.h"
#include "prefs.h"
#include "ssm.h"
#include <ncurses.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#define MSG_BUFSIZE 200
static char ecuscan_msg[MSG_BUFSIZE];
char *defaultSerialPort = "/dev/ttyS0";
_ecu* ecu;
int readErrors = 0;

user_prefs_type user_prefs = {
	.log_mode = LOG_MODE_ON,
	.log_level = DEBUG
};


/**
 * Renders the title and menu options
 */
void draw_screen() {
	clear();
	white_on_black();
	move(0, 28);
	printw("Subaru ECU Utility");
	move(1, 28);
	printw("==================");
	move(24, 1);
	printw("Q:Quit   T:Toggle C/F   M:Toggle MPH/KMH   L:Toggle Logging");
	refresh();
}

void Tempbar(int tempC) {
	int x,y,percent;

	if (tempC < 50) {
		black_on_cyan();
	} else if (tempC < 230) {
		black_on_green();
	} else  {
		black_on_red();
	}

	percent=((tempC+50)*100)/255;
	if (percent>100) percent=100;

	for (x=0;x<percent/5;x++) printw(" ");
	black_on_yellow();
	for (y=x;y<20;y++) printw(" ");
}


/**
 * Loads the buffer with a value from the ECU.
 *
 * @param  address      the address of the value to read from the ECU
 * @param  buffer       the location to load the value into
 * @param  description  a string describing the value
 * @param  convert      if non-null, a function which does a conversion on the value
 */
void query_ecu(int address, int* buffer, char* description, void (*convert)(int*)) {
    int rc = ssm_query_ecu(address, buffer, 1);
    if (rc != 0) {
        snprintf(ecuscan_msg, MSG_BUFSIZE, "Error querying ECU, ssm_query_ecu() returned %d. Read error %d", rc, ++readErrors);
        log_message(ecuscan_msg, ERROR);
    } else {
        readErrors = 0;
        if (convert) {
            convert(buffer);
        }
    }
}

bool leftColumn = true;
const int leftCol = 5;
const int rightColOffset = 42;
int row = 4;

void display_value(int address, int* buffer, char* description,
		void (*convertEcuValue)(int*), char *textValue(int*)) {
	query_ecu(address, buffer, description, convertEcuValue);

	int col;
	if (leftColumn) {
		col = leftCol;
	} else {
		col = leftCol + rightColOffset;
	}
	// Value title
	move(row, col + 1);
	white_on_black();
	printw(description);
	// Bar value
	move(row + 1, col);
	bar(50);
	// Text value
	move(row + 1, col + 20);
	white_on_black();
	printw("%s", textValue(buffer));
	// Refresh UI
	refresh();

	// Increment coordinates for next value
	leftColumn = !leftColumn;
	if (leftColumn) {
		row += 3;
	}
}

/** Convert ECU value for vehicle speed */
void convertVehicleSpeed(int* vehicleSpeed) {
    *vehicleSpeed = *vehicleSpeed * 2;
    if (!user_prefs.metric_speed) {
        *vehicleSpeed = (*vehicleSpeed * 5) / 8;
    }
}
char* convertVehicleSpeedText(int* value) {
    if (user_prefs.metric_speed) {
        snprintf(ecuscan_msg, MSG_BUFSIZE, "%3d kph ", *value);
    } else {
        snprintf(ecuscan_msg, MSG_BUFSIZE, "%3d mph ", *value);
    }
    return ecuscan_msg;
}

/** Convert ECU value for RPM */
void convertRPM(int* rpm) {
	*rpm *= 25;
}
char* convertRPMText(int* value) {
	snprintf(ecuscan_msg, MSG_BUFSIZE, "%4d rpm ", *value);
	return ecuscan_msg;
}

/** Convert ECU value for throttle position*/
void convertThrottlePosition(int* throttlePosition) {
	*throttlePosition *= (*throttlePosition * 500) / 255;
}
char* convertTPSText(int* value) {
	snprintf(ecuscan_msg, MSG_BUFSIZE, " %1d.%02dV (%3d%%)",
			*value / 100,
			*value % 100,
			*value / 5);
	return ecuscan_msg;
}

/** Convert ECU value for MAF */
void convertMAF(int* maf) {
	*maf = (*maf * 500) / 255;
}
char* convertMAFText(int* value) {
	snprintf(ecuscan_msg, MSG_BUFSIZE, " %1d.%02dV ",
			*value / 100,
			*value % 100);
	return ecuscan_msg;
}

void convertBatteryVoltage(int* batt) {
	*batt = *batt * 8;
}
char* convertBatteryVoltageText(int* value) {
	snprintf(ecuscan_msg, MSG_BUFSIZE, " %2d.%02dV ",
			*value / 100,
			*value % 100);
	return ecuscan_msg;
}

void convertCoolantTemp(int* temp) {
	*temp = *temp - 50;
	if (!user_prefs.metric_temp) {
		*temp = (*temp * 9) /5 + 32;
	}
}
char* convertCoolantTempText(int* value) {
	if (user_prefs.metric_temp) {
		snprintf(ecuscan_msg, MSG_BUFSIZE, " %3d C ", *value);
	} else {
		snprintf(ecuscan_msg, MSG_BUFSIZE, " %3d F ", *value);
	}
	return ecuscan_msg;
}

void convertO2(int* o2) {
	*o2 = *o2 * 5000 / 512;
}
char* convertO2Text(int* value) {
	snprintf(ecuscan_msg, MSG_BUFSIZE, " %3d mV ", *value);
	return ecuscan_msg;
}

void convertO2Min(int* o2) {
    *o2 = *o2 * 5000 / 512;
}
char* convertO2MinText(int* value) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, " %3d mV ", *value);
    return ecuscan_msg;
}

void convertO2Max(int* o2) {
    *o2 = *o2 * 5000 / 512;
}
char* convertO2MaxText(int* value) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, " %3d mV ", *value);
    return ecuscan_msg;
}

void convertAirFuel(int* af) {
    *af = *af - 128;
}
char* convertAirFuelText(int* value) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, " %d % ", *value);
    return ecuscan_msg;
}

void convertAtmosphericPressure(int* ap) {
    *ap = *ap * 8;
}
char* convertAtmosphericPressureText(int* value) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, " %d mmHg ", *value);
    return ecuscan_msg;
}

void convertBoostSolenoidDutyCycle(int *bsdc) {
    *bsdc = *bsdc * 100 / 256;
}
char* convertBoostSolenoidDutyCycleText(int *value) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, " %d % ", *value);
    return ecuscan_msg;
}

void convertEngineLoad(int *load) {
}
char* convertEngineLoadText(int *value) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, " %d ", *value);
    return ecuscan_msg;
}

void convertIgnitionAdvance(int *ignitionAdvance) {
}
char* convertIgnitionAdvanceText(int *value) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, " %d Deg BTDC ", *value);
    return ecuscan_msg;
}

void convertInjectorPulseWidth(int *pulseWidth) {
    *pulseWidth = *pulseWidth * 256 / 1000;
}
char* convertInjectorPulseWidthText(int *value) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, " %d ms ", *value);
    return ecuscan_msg;
}

void convertIAC(int *iac) {
    *iac = *iac * 100 / 256;
}
char* convertIACText(int *value) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, " %d % ", *value);
    return ecuscan_msg;
}

void convertKnockCorrection(int *knockCorrection) {
}
char* convertKnockCorrectionText(int *value) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, " %d % ", *value);
    return ecuscan_msg;
}

void convertManifoldPressure(int *pressure) {
    *pressure = ( *pressure - 128 ) / 85;
}
char* convertManifoldPressureText(int *value) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, " %d bar ", *value);
    return ecuscan_msg;
}

// Presumes a two-column layout
void display_data() {
	row = 4;
	leftColumn = true;

	/***************************************************/
	// Reusable values here are:
	// (int address, int* buffer, char* description, void (*convert)(int*))
	/***************************************************/
	display_value(ecu->vehicleSpeed.romAddress, &ecu->vehicleSpeed.data, "Vehicle Speed", convertVehicleSpeed, convertVehicleSpeedText);
	display_value(ecu->rpm.romAddress, &ecu->rpm.data, "Engine Speed", convertRPM, convertRPMText);
	// TODO: why is TPS not converted here?
	// display_value(ecu->throttlePosition.romAddress, &ecu->throttlePosition.data, "Throttle Position", convertThrottlePosition, convertTPSText);
	display_value(ecu->throttlePosition.romAddress, &ecu->throttlePosition.data, "Throttle Position", NULL, convertTPSText);
	display_value(ecu->massAirFlow.romAddress, &ecu->massAirFlow.data, "Airflow Sensor", convertMAF, convertMAFText);
	display_value(ecu->batteryVoltage.romAddress, &ecu->batteryVoltage.data, "Battery Voltage", convertBatteryVoltage, convertBatteryVoltageText);
	display_value(ecu->coolantTemp.romAddress, &ecu->coolantTemp.data, "Coolant Temperature", convertCoolantTemp, convertCoolantTempText);
	display_value(ecu->o2Average.romAddress, &ecu->o2Average.data, "O2 Sensor", convertO2, convertO2Text);
    display_value(ecu->airFuelCorrection.romAddress, &ecu->airFuelCorrection.data, "Air/fuel Trim", convertAirFuel, convertAirFuelText);
    display_value(ecu->atmosphericPressure.romAddress, &ecu->atmosphericPressure.data, "Atmospheric Pressure", convertAtmosphericPressure, convertAtmosphericPressureText);
    display_value(ecu->boostSolenoidDutyCycle.romAddress, &ecu->boostSolenoidDutyCycle.data, "Boost Solenoid duty cycle", convertBoostSolenoidDutyCycle, convertBoostSolenoidDutyCycleText);
    display_value(ecu->engineLoad.romAddress, &ecu->boostSolenoidDutyCycle.data, "Engine Load", convertEngineLoad, convertEngineLoadText);
    display_value(ecu->ignitionAdvance.romAddress, &ecu->ignitionAdvance.data, "Ignition Advance", convertIgnitionAdvance, convertIgnitionAdvanceText);
    display_value(ecu->injectorPulseWidth.romAddress, &ecu->injectorPulseWidth.data, "Injector Pulse Width", convertInjectorPulseWidth, convertInjectorPulseWidthText);
    display_value(ecu->isuDutyValve.romAddress, &ecu->isuDutyValve.data, "IAC Duty Cycle", convertIAC, convertIACText);
    display_value(ecu->knockCorrection.romAddress, &ecu->knockCorrection.data, "Knock Correction", convertKnockCorrection, convertKnockCorrectionText);
    display_value(ecu->manifoldPressure.romAddress, &ecu->manifoldPressure.data, "Manifold Relative Pressure", convertManifoldPressure, convertManifoldPressureText);
    display_value(ecu->o2Min.romAddress, &ecu->o2Min.data, "O2 Min", convertO2Min, convertO2MinText);
    display_value(ecu->o2Max.romAddress, &ecu->o2Max.data, "O2 Max", convertO2Max, convertO2MaxText);

	print_logmode(user_prefs.log_mode, 19, 35);

	log_ecu(ecu);
}

_ecu* open_ssm(char *serialPort) {
    snprintf(ecuscan_msg, MSG_BUFSIZE, "Opening serial port %s", serialPort);
    log_message(ecuscan_msg, INFO);
    int rc;
    if ((rc = ssm_open(serialPort)) != 0) {
        snprintf(ecuscan_msg, MSG_BUFSIZE, "ssm_open(\"%s\") returned %d.\n"
                "Does the device file exist? Are you running with `sudo`?",
                serialPort, rc);
        log_message(ecuscan_msg, ERROR);
        fprintf(stderr, "%s\n", ecuscan_msg);
        perror("Error Details");
        // If we tried opening the default serial port, try /dev/ttyUSB0 instead
        if (serialPort == defaultSerialPort) {
            snprintf(ecuscan_msg, MSG_BUFSIZE, "Trying /dev/ttyUSB0 instead");
            log_message(ecuscan_msg, INFO);
            fprintf(stderr, "%s\n", ecuscan_msg);
            return open_ssm("/dev/ttyUSB0");
        }
        exit(1);
    }
    return load_ecu_settings();
}

// Should be the last atexit() hook run
void finish() {
	log_message("Exiting\n\n", INFO);
}

int main(int argc, char *argv[]) {
	char *serialPort = defaultSerialPort;
	int opt, height, width, need_redraw = 1, keypress = 0;

	while ((opt = getopt (argc, argv, "d:h")) != -1 ) {
		switch(opt) {
			case 'd': serialPort = optarg;
					printf("Using serial port %s\n", optarg);
					break;
			case 'h':
			default : printf("Got opt %d\n", opt);
					printf("Usage: %s [OPTION]\n", argv[0]);
					printf("Display current Subaru ECU parameters over the SSM1 protocol.\n\n");
					printf("  -d          device file, defaults to /dev/ttyS0\n");
					printf("                for a USB-to-serial adapter, try /dev/ttyUSB0\n");
					printf("  -h          display this help and exit\n");
					exit(0);
		}
	}

	// Register final shutdown hook
	atexit(finish);

	// Initialize logs
	init_logs(&user_prefs);
	atexit(close_logs);

	// Initialize ECU connection
	ecu = open_ssm(serialPort);
    if (!ecu) {
		exit(-1);
    }
    atexit((void *) ssm_close);

	// Initialize curses
	initscr();
	atexit(cleanup_curses);
	curs_set(0);
	noecho();
	setup_colours();
	nodelay(stdscr,TRUE);

	while((keypress & 0xDF) != 'Q') {
		getmaxyx(stdscr,height,width);
		if ((height < 25) || (width < 80)) {
			if (need_redraw == 0) {
				clear();
				need_redraw=1;
			}
			screen_too_small();
		}
		else {
			if (need_redraw == 1) {
				draw_screen();
				need_redraw= 0;
			}
			display_data();
		}
		keypress = getch();
		if ((keypress & 0xDF) == 'T') user_prefs.metric_temp = !user_prefs.metric_temp;
		if ((keypress & 0xDF) == 'M') user_prefs.metric_temp = !user_prefs.metric_temp;
		if ((keypress & 0xDF) == 'L') {
			if (user_prefs.log_mode == LOG_MODE_ON) {
				user_prefs.log_mode = LOG_MODE_OFF;
			} else if (user_prefs.log_mode == LOG_MODE_OFF) {
				user_prefs.log_mode = LOG_MODE_ON;
			}
		}
	}
}
