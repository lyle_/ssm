# ssm: Subaru Select Monitor interface

This software enables access to pre-OBDII Subaru ECU computers for metrics and
monitoring purposes.

Adapted from [http://www.alcyone.org.uk/ssm/software.html](http://www.alcyone.org.uk/ssm/software.html).

## Building

### Requirements
- A `C99` compiler
- libncurses

### Build targets
Run `make` to build the project; binaries will be created in the `bin` directory.

Run `make check` to run unit tests (requires the [check](https://libcheck.github.io/check/)
framework to be installed).

`make clean` removes compiled executables, object files, and tests.


## Running

Note that these utilities may need additional permissions in order to access the
serial port device; if you get a `Permission denied` error, try running the command
with `sudo`.

### ecuscan - View and log ECU parameters

`ecuscan` tries to connect to a Subaru SSM port via the first serial port, `/dev/ttyS0`.
If that isn't the serial port you're using, you can specify the optional `-d`
parameter. For example, if you're using a USB-to-serial adapter based on the FTDI
chip, you'd likely want to run something like `ecuscan -d /dev/ttyUSB0`.

The application displays ECU values but does not log any data by default,
if you toggle logging on via the 'L' key, the "Not Logging" text should change to "Logging" and ECU data will
be streamed to `ecuscan.csv` in the current directory.

Debugging data for each run will be logged to `ecuscan.log` in any case.


## Development

### Source files (in ./src)
	checkecu.c	Read or Update checksum in modified ECU ROM file
	curses.c	Utilities for drawing the TUI with curses
	ecudump.c	Download ECU memory to a file
	ecuscan.c	View and log ECU parameters
	logging.c	Utilities for writing log output
	simulated_ssm.c	Runs UI with simulated ECU
	ssm.c	Select Monitor Communications Library

### Generating JSON compilation database

In order to assist various linting utilities, the build process will generate a
[JSON Compilation Database](https://clang.llvm.org/docs/JSONCompilationDatabase.html)
if the [bear](https://github.com/rizsotto/Bear) utility is available.

### UI Debugging

A version of `ecuscan` called `simulate_ecuscan` is compiled which will run with
simulated inputs for ECU values; this makes developing functionality which
isn't dependent on an actual ECU connection much more convenient.

## Resources

* This project is based on the excellent work of Phil Skuse at: http://www.alcyone.org.uk/ssm/index.html
* Overview of SSM parameters: http://www.4bc.org/vanagon/engine.html
* Scaling data values read from the ECU: https://sourceforge.net/p/selectmonitor/code-0/HEAD/tree/trunk/SelectMonitor.pas#l198
* Memory addresses for various ROM revisions: http://www.vwrx.com/selectmonitor/SelectMonitor.ini
* Memory addresses defined by FreeSSM: https://github.com/Comer352L/FreeSSM/blob/master/definitions/SSM1defs_Engine.xml
