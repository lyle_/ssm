/**
 * ssm.h - Subaru Select Monitor communications library
 *
 * Copyright 2007, 2018 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
#pragma once

/*=====================================================================*/
/* ALL OF THESE FUNCTIONS RETURN ZERO ON SUCCESS AND NON-ZERO ON ERROR */
/*=====================================================================*/


/*---------------------------------------------------------------------*/
/*  ssm_open() opens the COM port and configures it to talk to the car */
/*---------------------------------------------------------------------*/
int ssm_open(char *device);

/*------------------------------------------------*/
/* ssm_reset() tells the car to stop sending data */
/*------------------------------------------------*/
int ssm_reset();

/*--------------------------------------------------------------------------*/
/* ssm_query_ecu() tells the ECU to start sending the contents of a given   */
/* address in it's RAM. It will read the address n times and store the data */
/* in the buffer so that you can analyse how the data changes over time.    */
/*--------------------------------------------------------------------------*/
int ssm_query_ecu(int address, int *buffer, int n);

/*-----------------------------------------------------*/
/* ssm_current() returns the data currently being sent */
/*-----------------------------------------------------*/
int ssm_current(int *address, int *data);
	
/*---------------------------------------*/ 
/* ssm_close() shuts down the connection */
/*---------------------------------------*/ 
int ssm_close();

/*--------------------------------------------------*/
/* ssm_write_ecu() writes a byte of data to the ECU */
/*--------------------------------------------------*/
int ssm_write_ecu(int address,unsigned char data);

/*-----------------------------------------------*/
/* ssm_romid_ecu() returns the ROM ID of the ECU */
/*-----------------------------------------------*/
int ssm_romid_ecu(int *romid);
