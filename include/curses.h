/**
 * curses.h - curses-related TUI functionality
 *
 * Copyright 2018 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
#pragma once

#include "logging.h"

/*---------------------------------------------------------------------*/
//  Initialize curses environment                                      */
/*---------------------------------------------------------------------*/
void init_curses();

/*---------------------------------------------------------------------*/
//  Restores the terminal to its normal state.                         */
/*---------------------------------------------------------------------*/
void cleanup_curses();

/*---------------------------------------------------------------------*/
//  Warns user if terminal size is insufficient to display data.       */
/*---------------------------------------------------------------------*/
void screen_too_small();

/*=================================*/
/* Functions to handle colour text */
/*=================================*/

void setup_colours();
void white_on_black();
void black_on_green();
void black_on_yellow();
void black_on_red();
void black_on_cyan();

/*===============================================*/
/* bar() draws a bar graph of a given percentage */
/*===============================================*/
void bar(int percent);

/*===============================================*/
/* Render the current logging mode               */
/*===============================================*/
void print_logmode(log_mode_type logmode, int row, int col);
