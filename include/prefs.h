/**
 * prefs.h - User preferences
 *
 * Copyright 2007, 2018 by it's authors.
 * Some rights reserved. See COPYING, CREDITS
 */
#pragma once
#include <stdbool.h>

/** Whether to write out ECU values to a data log */
typedef enum { LOG_MODE_OFF, LOG_MODE_ON, LOG_MODE_ERROR } log_mode_type;
/** Level of application logging */
typedef enum { TRACE, DEBUG, INFO, WARN, ERROR } log_level_type;

typedef struct user_prefs {
    bool metric_speed;
    bool metric_temp;
    log_mode_type log_mode;
    log_level_type log_level;
} user_prefs_type;
